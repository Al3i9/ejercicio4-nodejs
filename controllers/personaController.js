const { guardarDB, getDB } = require('../helpers/guardarDB');
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const { response } = 'express';

const personas = new Personas();
  
const personasGet = (req, res = response) => {

  const personasDB =  getDB();
   if (personasDB) {
     personas.cargarPersonasEnArray(personasDB);
   }
  // let array =   personas._listado;
  res.json({
    personasDB
  });
};

const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
  //actualizando la lista 
  let personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  personas.crearPersona(persona);
  // console.log(listado);
  guardarDB(personas.listadoArr);
  personasDB = getDB();
  res.json({
    msg: 'post API - Controlador',
    personasDB
  });
};

const personaPut = (req, res = response) => {
  //actualiza lista
  const personasDB =  getDB();
   if (personasDB) {
     personas.cargarPersonasEnArray(personasDB);
   }
  const {id} = req.params;
  if (id) {
    personas.borrarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
    persona.setID(id);
    personas.crearPersona(persona);
    guardarDB(personas.listadoArr);
  }
  res.json({
    msg: 'put API - Controlador'
  });
};

//Crear y mandar informacion  con POST:

const personaDelete = (req, res = response) => {
  const personasDB =  getDB();
   if (personasDB) {
     personas.cargarPersonasEnArray(personasDB);
   }
  const {id} = req.params;
  // console.log(id);
  if (id) {
    personas.borrarPersona(id);
    guardarDB(personas.listadoArr);
     //console.log(personas._listado);
  }
  res.json({
    msg: 'delete API - Controlador'
  });
};

module.exports = {
  personasGet,
  personaPut,
  personaPost,
  personaDelete
}